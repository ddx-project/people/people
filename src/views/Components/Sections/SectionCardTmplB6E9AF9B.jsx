import React from "react";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// core components
import cardStyle from "assets/jss/material-kit-react/views/componentsSections/cardStyle.jsx";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";

import { OipApi } from "oip/OipApi";
import { config } from "ddx.config.js";

const api = new OipApi(config.daemonApiUrl);

class SectionCardTmplB6E9AF9B extends React.Component {
  state = {
    name: "",
    description: "",
    address: "",
    thumbnail: require("assets/img/ddx-placeHolder.png")
  };

  componentDidMount() {
    if (this.props.data) {
      const recordInfo = this.props.data.record.details;
      console.log(recordInfo);
      if (recordInfo) {
        const avatarId =
          recordInfo[config.cardInfo.avatarRecord.tmpl][
            config.cardInfo.avatarRecord.name
          ];
        const callAvatar = api.getRecord(avatarId);

        let name =
          recordInfo[config.cardInfo.name.tmpl][config.cardInfo.name.name];

        if (recordInfo[config.cardInfo.surname.tmpl]) {
          name +=
            recordInfo[config.cardInfo.surname.tmpl][
              config.cardInfo.surname.name
            ];
        }

        const description =
          recordInfo[config.cardInfo.description.tmpl][
            config.cardInfo.description.name
          ];

        this.setState({
          name,
          description
        });

        callAvatar.then(response => {
          if (response !== "not found") {
            const address =
              response.results[0].record.details[
                config.imageHandler.thumbnail.tmpl
              ][config.imageHandler.thumbnail.name];
            this.setState({
              thumbnail: `${config.ipfs.apiUrl}${address}`
            });
          }
        });
      }
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <Card className={classes.card} href="/record">
        <CardActionArea>
          <CardMedia
            component="img"
            alt="Record Image"
            className={classes.media}
            height="150"
            image={this.state.thumbnail}
            title={this.state.name}
          />
          <CardContent>
            <Typography noWrap={true} variant="h6">
              {this.state.name}
            </Typography>
            <Typography noWrap={true}>{this.state.description}</Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Button size="small" color="primary">
            Share
          </Button>
          <Button size="small" color="primary" href="/record">
            View
          </Button>
        </CardActions>
      </Card>
    );
  }
}

export default withStyles(cardStyle)(SectionCardTmplB6E9AF9B);
